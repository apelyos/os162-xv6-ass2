#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

void sig_handl(int pid, int value){
  printf(1, "my pid %d, the sender pid is %d, the value sent is %d\n" ,getpid(), pid, value);

  //sigret();
}

int main() {
  printf(1, "new sig %p:\n", sig_handl);
  sig_handler old = sigset(sig_handl);
  printf(1, "old sig %p:\n", old);
  //printf(1, "sighand %d:\n" , (uint)&sig_hand);
  printf(1, "starting test:\n");
  printf(1, "my pid %d\n", getpid());
  int pid = fork();
  //printf(1, "fork!!!!!!!!!1 %d\n", pid);

  if(pid != 0){ //father
    printf(1, "father!!!! son: %d\n", pid);
    sleep(100);
    sigsend(pid, 555);
    printf(1, "sendsig\n");
    //wait();
  } else {

    sigpause();
   printf(1, "son!!!!!! \n");
   //while(1) {
     //sleep(1000);
     //printf(1, "sleeping");
   //}
   exit();
  }

  printf(1, "father done\n");
  exit();
}

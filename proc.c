#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

#define SIGSEND_RET(ret) (ret - 1)

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);
extern void injected_sigret(void);
extern void injected_sigret_end(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

int
allocpid(void)
{
  int old;
  do {
    old = nextpid;
  } while (!cas(&nextpid, old, old+1));

  return nextpid;
}
//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;
  int i;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    // check if p is unused & take it!
    if(cas(&p->state, UNUSED, EMBRYO))
      goto found;
  return 0;

found:
  // p is already embryo here
  p->pid = allocpid();

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;

  //initiate all frames
  for(i = 0; i < 10 ; ++i)
    p->pending_signals.frames[i].used = 0;
  p->pending_signals.head = 0;
  p->sig_hand = (sig_handler)-1;
  p->signal_in_progress = 0;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];

  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  //p->state = RUNNABLE;
  if (!cas(&p->state, EMBRYO, RUNNABLE))
    panic("init - trans to runble");
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n) // sbrk
{
  uint sz;

  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    np->state = UNUSED;
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *np->tf = *proc->tf;
  //new- signals
  np->sig_hand = proc->sig_hand;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));

  pid = np->pid;

  // lock to force the compiler to emit the np->state write last.
  //acquire(&ptable.lock); // new
  pushcli();
  if (!cas(&np->state, EMBRYO, RUNNABLE))
    panic("fork - trans to runble");
  //np->state = RUNNABLE;
  popcli();
  //release(&ptable.lock); // new

  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
  struct proc *p;
  int fd;
  struct proc *curr_proc = proc;

  if(proc == initproc)
    panic("init exiting");


  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  //acquire(&ptable.lock); // new
  pushcli();
  // new: mark in transition
  //proc->state = TRANS_ZOMBIE;
  if (!cas(&proc->state, RUNNING, TRANS_ZOMBIE))
    panic("trans to zombie");

  // Parent might be sleeping in wait().
  //wakeup1(proc->parent); //moved to scheduler

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == curr_proc &&
      cas((int*)&p->parent, (int)curr_proc, (int)initproc)){
      //p->parent = initproc; // already done
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }
  //proc->state = ZOMBIE;
  // Jump into the scheduler, never to return.

  sched();
  panic("zombie exit");
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  //acquire(&ptable.lock); // new
  pushcli();
  for(;;){
    //proc->state = TRANS_SLEEPING; // mark as trans state
    if (!cas(&proc->state, RUNNING, TRANS_SLEEPING))
      panic("wait - trans to sleep");
    //if (!cas(&proc->state, RUNNING, TRANS_SLEEPING))
    //  panic("trans to sleep");
    proc->chan = (int)proc;
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      //while(p->state == TRANS_ZOMBIE); // busy wait while in trans // maybe unneccessary
      if(p->state == ZOMBIE && cas(&p->state, ZOMBIE, TRANS_UNUSED)){
        // Found one.
        // mark in trans
        //p->state = TRANS_UNUSED; //already done
        pid = p->pid;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->state = UNUSED;

        proc->chan = 0;
        //proc->state = RUNNING;
        if (!cas(&proc->state, TRANS_SLEEPING, RUNNING))
          panic("wait - trans to rning 1");
        //release(&ptable.lock);  // new
        popcli();
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      proc->chan = 0;
      //proc->state = RUNNING;
      if (!cas(&proc->state, TRANS_SLEEPING, RUNNING))
        panic("wait - trans to rning 2");
      //release(&ptable.lock);  // new
      popcli();
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sched();
  }
}

void
freeproc(struct proc *p)
{
  if (!p || (p->state != ZOMBIE && p->state != TRANS_ZOMBIE)) // new
    panic("freeproc not zombie");
  kfree(p->kstack);
  p->kstack = 0;
  freevm(p->pgdir);
  p->killed = 0;
  p->chan = 0;
}

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
  struct proc *p;

  for(;;){
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    //acquire(&ptable.lock);
    pushcli();
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      //if(p->state != RUNNABLE) // new
      if (!(p->state == RUNNABLE && cas(&p->state, RUNNABLE, RUNNING)))
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
      switchuvm(p);
      //p->state = RUNNING; //already done above
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.

      if (p->state == TRANS_ZOMBIE ) {
        freeproc(p);
        p->state = ZOMBIE;
        wakeup1(p->parent);
      }

      // new: if it's in transition, mark it as complete
      if (proc->state % 2 == 1)
        --proc->state;

      proc = 0;
    }
    //release(&ptable.lock);
    popcli();

  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
  int intena;

  //if(!holding(&ptable.lock)) // new
  //  panic("sched ptable.lock"); // new
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING || proc->state == TRANS_RUNNING) // new
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}

// Give up the CPU for one scheduling round.
void
yield(void)
{
  //acquire(&ptable.lock);  //DOC: yieldlock // new
  pushcli();
  if (!cas(&proc->state, RUNNING, TRANS_RUNNABLE))
    panic("yield - trans");
  //proc->state = TRANS_RUNNABLE; // mark as in transition
  sched();
  //release(&ptable.lock); // new
  popcli();
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  //release(&ptable.lock); // new
  popcli();

  if (first && cas(&first, 1, 0)) { //new
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    //first = 0; // done above
    initlog();
  }

  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  if(lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  //if(lk != &ptable.lock){  //DOC: sleeplock0
      //cprintf("aq sleep %d\n", proc->state);
  //  acquire(&ptable.lock);  //DOC: sleeplock1

  pushcli();
  //proc->state = TRANS_SLEEPING;
  if (!cas(&proc->state, RUNNING, TRANS_SLEEPING))
    panic("sleep - trans");
  release(lk);
  //}

  proc->chan = (int)chan;

  sched();

  // Reacquire original lock.
  //if(lk != &ptable.lock){  //DOC: sleeplock2
  //  release(&ptable.lock);
  popcli();
  acquire(lk);
  //}
}

//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->chan == (int)chan && (p->state == SLEEPING || p->state == TRANS_SLEEPING)){
      while (p->state == TRANS_SLEEPING); // busy wait while in trans
      if (cas(&p->state, SLEEPING, TRANS_RUNNABLE)){
        // Tidy up.
        p->chan = 0;
        //p->state = RUNNABLE;
        if (!cas(&p->state, TRANS_RUNNABLE, RUNNABLE))
          panic("wakeup1 - to runble");
      }
    }
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)

{
  //acquire(&ptable.lock);
  pushcli();
  wakeup1(chan);
  //release(&ptable.lock);
  popcli();
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
  struct proc *p;
  //cprintf("in kill\n");
  //acquire(&ptable.lock);
  pushcli();
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid && cas(&p->killed, 0, 1)){
      //p->killed = 1; done above
      // Wake process from sleep if necessary.
      //if(p->state == SLEEPING)
      //  p->state = RUNNABLE;
      while(p->state == TRANS_SLEEPING);
      cas(&p->state, SLEEPING, RUNNABLE);
      //release(&ptable.lock);
      popcli();
      return 0;
    }
  }
  //release(&ptable.lock);
  popcli();
  return -1;
}

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  static char *states[] = {
  [UNUSED]    "unused",
  [TRANS_UNUSED]    "trans_unused",
  [EMBRYO]    "embryo",
  [TRANS_EMBRYO]    "trans_embryo",
  [SLEEPING]  "sleep ",
  [TRANS_SLEEPING]    "trans_sleep",
  [RUNNABLE]  "runble",
  [TRANS_RUNNABLE]    "trans_runble",
  [RUNNING]   "run   ",
  [TRANS_RUNNING]    "trans_runing",
  [ZOMBIE]    "zombie",
  [TRANS_ZOMBIE]    "trans_zombie",
  };
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }

}


// new -signals ********************

// adds a new frame to the cstack which is initialized with values
// sender_pid , recepient_pid and value , then returns 1 on success and 0
// if the stack is full
int push (struct cstack *cstack , int sender_pid , int recepient_pid , int value){
  int i = 0;

  for(; i < 10; ++i){
    if(cas(&cstack->frames[i].used, 0, 1))
      break;
  }
  if (i == 10)
    return 0;

  cstack->frames[i].sender_pid = sender_pid;
  cstack->frames[i].recepient_pid = recepient_pid;
  cstack->frames[i].value = value;

  while(1){
    cstack->frames[i].next = cstack->head;
    if(cas((int*)&cstack->head, (int)cstack->frames[i].next ,(int)&cstack->frames[i]))
      break;
  }

  return 1;
}

// removes and returns an element from the head of given cstack
// if the stack is empty , then return 0
// the used filed stays 1, the user need to change it when is done using
struct cstackframe *pop (struct cstack *cstack){
  struct cstackframe * head;

  if (cstack->head == 0)
    return 0;

  while(1){
    head = cstack->head;
    if(cas((int*)&cstack->head , (int)head,  (int)head->next))
      break;
  }

  return head;
}

sig_handler sigset(sig_handler new_handler){
  sig_handler old_handler = proc->sig_hand;
  proc->sig_hand = new_handler;
  return old_handler;
}

int sigsend(int dest_pid, int value){
  struct proc *p;
  int push_ret;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == dest_pid)
      break;
  }
  if (p >= &ptable.proc[NPROC])
    return -1; /*means that there is no proc with this pid*/

  push_ret = push(&p->pending_signals, proc->pid, dest_pid, value);
  wakeup(&p->pending_signals);
  return SIGSEND_RET(push_ret);
}

void sigret(void) {
  // restore old TF
  memmove(proc->tf, &proc->sig_tf, sizeof(struct trapframe));

  proc->signal_in_progress = 0;
}

int sigpause(void){
  //proc->state = TRANS_SLEEPING; //trans?
  if (!cas(&proc->state, RUNNING, TRANS_SLEEPING))
    panic("sigpause - to trans slep");
  proc->chan = (int)&proc->pending_signals; // sleep on

  // call sched by convention
  pushcli();
  sched();
  popcli();

  return 0;
}

void invoke_signals(void) {
  struct cstackframe *frame;
  int inj_size;
  uint orig_esp;

  if (proc == 0)
    return;

  if ((proc->tf->cs&3) == 0) // if in kernel mode - quit
    return;

  if (proc->signal_in_progress == 1)
    return;

  proc->signal_in_progress = 1;

  if ((frame = pop(&proc->pending_signals)) == 0) {
    proc->signal_in_progress = 0;
    return;
  }

  if ((int)proc->sig_hand == -1) {
    proc->signal_in_progress = 0;
    return;
  }
  memmove(&proc->sig_tf, proc->tf, sizeof(struct trapframe));  /*backup frame*/

  inj_size = (uint)injected_sigret_end - (uint)injected_sigret;

  (proc->tf->esp) -= inj_size;

  orig_esp = proc->tf->esp;
  // copy compiled code from injected_sigret.S
  memmove((uint*)proc->tf->esp, injected_sigret, inj_size);

  proc->tf->eip = (uint)proc->sig_hand; // modify next inst to SIG_HANDLER

  (proc->tf->esp) -= 4;
  *((int*)proc->tf->esp) = frame->value; // second param

  (proc->tf->esp) -= 4;
  *((int*)proc->tf->esp) = frame->sender_pid; // first param

  (proc->tf->esp) -= 4;
  *((uint*)proc->tf->esp) = orig_esp; // ret addr (injected)

  frame->used = 0;

}

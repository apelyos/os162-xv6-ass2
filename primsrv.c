#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

#define MAX_WORKERS 1000
#define MAX_CHAR_FOR_NUMBER 10

typedef struct data{
  int pid;
  int used;
  int number;
} worker_data;

int is_prime(int number){
  int i;

  for (i=2; i<number; i++)
    if (number % i == 0 && i != number)
      return 0;
  return 1;
}

worker_data workers_data[MAX_WORKERS];

void worker_handler(int pid, int value){
  int ret_val ,my_pid = getpid();
  int prime = 0;

  if (value == 0){
    printf(1, "worker %d exit\n", my_pid);
    exit();
  }

  for(ret_val = value + 1 ; ; ret_val++){
    prime = is_prime(ret_val);
    if (prime)
      break;
    }
  sigsend(pid, ret_val);
}

void srv_handler(int pid, int value){
  int i;

  for(i = 0; i < MAX_WORKERS; ++i){
    if(workers_data[i].pid == pid)
      break;
  }
  printf(1 ,"worker %d returned %d as a result for %d\n" ,pid , value ,workers_data[i].number);
  workers_data[i].used = 0;
}


int main(int argc, char *argv[])
{
  int pid;
  int i, input, n = 0;
  char buffer[MAX_CHAR_FOR_NUMBER];

  // check args
  if (argc < 2) {
    printf(1, "Incorrect arg count.\n");
    exit();
  }

  n = atoi(argv[1]);

  printf(1, "workers pids:\n");

  for (i = 0 ; i < n ; ++i) {
    pid = fork();

    if (pid == -1) {
      printf(1, "PANIC: fork() didn't like this. Exiting.\n");
      exit();
    }

    // check if child
    if (pid != 0) {
      printf(1, "%d\n", pid);
      workers_data[i].pid = pid;
      workers_data[i].used = 0;
    } else {
      sigset(worker_handler);
      while(1)
        sigpause();
    }
  }
  
  sigset(srv_handler);
  while(1){//main loop
    printf(1, "please enter a number: ");
    read(0, buffer, MAX_CHAR_FOR_NUMBER);

    if(buffer[0] != 0 && buffer[0] != '\n'){
      input = atoi(buffer);
      if (input == 0){
        for(i = 0; i < n; ++i)
          sigsend(workers_data[i].pid, input);
        for(i = 0; i < n; ++i)
          wait();
          printf(1, "primsrv exit\n");
          exit();
      }

      for(i = 0; i < n; ++i)
        if(workers_data[i].used == 0)
          break;

      if(i == n){
	      printf(1, "no idle workers\n");
        continue;
      } else {
        sigsend(workers_data[i].pid, input);
        workers_data[i].used = 1;
        workers_data[i].number = input;
      }
    } else{
      sleep(1);
    }
  }
}
